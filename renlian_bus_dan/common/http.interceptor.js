const install = (Vue, vm) => {
	// let HOST = 'https://baozhen.hr-developer.com';
	let HOST = 'https://ymtgbzfw.cn/'
	let apiHost = HOST + "/api.php"
	// let apiHost = HOST+'/api'
	Vue.prototype.$u.http.setConfig({
		host:HOST,
		baseUrl: apiHost,
		dataType: 'json',
		header: {
			'Cache-Control'		:	'no-cache',
			'Content-Type'		:	'application/x-www-form-urlencoded',
			'XX-Program-name'	:	'gaobaozhen',
			'XX-Device-Type'	:	'wxapp',
			'XX-Api-Version'	:	'1.0.0',
			'XX-Token'			:	uni.getStorageSync('token'),
			'Access-Control-Allow-Origin':'*',
			'XX-Wxapp-AppId'	:	'wx236b2382695f3a03'
		},
		originalData: false,
		showLoading: true, // 是否显示请求中的loading
		loadingText: '请求中', // 请求loading中的文字提示
		loadingTime: 300, // 在此时间内，请求还没回来的话，就显示加载中动画，单位ms
		loadingMask: true, // 展示loading的时候，是否给一个透明的蒙层，防止触摸穿透
	});
	// 请求拦截，配置Token等参数
	Vue.prototype.$u.http.interceptor.request = (config) => {
		if(uni.getStorageSync('token')){
			config.header['XX-Token'] = uni.getStorageSync('token');
		}
		return config; 
	}
	// 响应拦截，判断状态码是否通过
	Vue.prototype.$u.http.interceptor.response = (res) => {
		//登录状态拦截
		let routes = getCurrentPages();
		if(routes[routes.length - 1]){
			let curRoute = routes[routes.length - 1].route
			if(res.code == 10001){
				if(curRoute != 'pages/user/login'){
					// if(getApp().globalData.versionStatus == 0){
						
					// }else{
					// 	uni.navigateTo({
					// 		url:"/pages/user/login" 
					// 	})
					// }
					uni.navigateTo({
						url:"/pages/user/login" 
					})
				}
			}
		}
		return res;
	}
}

export default {
	install
}