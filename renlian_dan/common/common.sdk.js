export default {
	
	
	// 下拉刷新上拉加载公共数据
	downOption:{
		auto:true
	},
	upOption:{
		auto:true,
		page: {
			num: 0,
			size: 15
		},
		noMoreSize: 10,
		empty:{
			tip: '~ 空空如也 ~', // 提示
			// btnText: '去看看'
		},
		textNoMore:"-- END --"
	},
	
	
	// 图片查看器
	previewImage(urls,current){
		
		uni.setStorageSync('tempimage',urls)
		uni.navigateTo({
			url:"/pages/fullscreen/image?current="+current,
			animationType: 'zoom-fade-out',
			animationDuration: 200
		})
	},
	
	// 观看视频
	playVideo(url){
		uni.navigateTo({
			url:"/pages/fullscreen/video?url="+url,
			animationType: 'zoom-fade-out',
			animationDuration: 200
		})
	},
	
	// 返回上一页
	goBack(){
		uni.navigateBack({
			delta:1
		})
	},
	
	
	random(n){
		var str = [
			'0','1','2','3','4','5','6','7','8','9',
			'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
		];
		var res = "";
		for(var i = 0; i < n ; i ++) {
			var id = Math.ceil(Math.random()*35);
			res += str[id];
		}
		return res;
	},
	
	navigateTo(url,params){
		console.log(url);
		uni.$u.route({
			url:url,
			params:params
		})
	},
	
	// 微信公众号H5登录
	h5login(options){
		let islogin = uni.getStorageSync('login');
		if(!islogin){
			if(!options.code){
				if(location.host != 'localhost:8080'){
					window.location.href = "http://" + location.host + "/plugin/wxlogin/index/wxlogin";
				}
			}else{
				uni.$u.http.get("plugin/wxlogin/index/h5login",{
					code:options.code
				}).then(res => {
					if(res.code){
						uni.setStorageSync('token',res.data.token);
						uni.setStorageSync('user',res.data.user);
						uni.setStorageSync('login',1);
						setTimeout(res => {
							uni.switchTab({
								url:"/pages/index/index"
							})
						},2000)
					}
				});
			}
		}
	}
	
}