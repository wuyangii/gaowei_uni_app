import md5 from '@/utils/md5.js';
import toast from '@/uview-ui/libs/function/toast.js';
export default{
	// 微信小程序支付
	payData:{},
	
	pay(options){
		
		this.payData = options;
		console.log(options);
		console.log(options.url + "/prepay");
		uni.$u.http.get(options.url + "/prepay",options.data).then(res => {
			var data = res.data;
			if(res.errmsg){
				uni.$u.toast(res.errmsg);
				return false;
			}
			if(!res.code && res.code != undefined){
				uni.$u.toast(res.msg);
				return false;
			}
			if(this.payData.provider == "weixin"){
				uni.$u.http.post(options.url + '/pay',{
					prepay_id: res.data.prepay_id,
				}).then( _payResult => {
					var payResult = _payResult.data;
					payResult.prepayid = res.data.prepay_id;
					this.wxappPay(payResult);
				})
			}
			if(this.payData.provider == "alipay"){
				this.aliappPay(res);
			}
		}).catch(res => {
			console.log(res);
		})
		
	},
	
	aliappPay(res){
		
		console.log(123);
		console.log(res);
		console.log(456);
		uni.requestPayment({
			provider: 'alipay',
			orderInfo: res.data, // 订单数据
			// timeStamp: data.timeStamp.toString(), // 时间戳从1970年1月1日至今的秒数，即当前的时间
			// nonceStr: data.nonceStr, // 随机字符串，长度为32个字符以下
			// package: data.package, // 统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=xx
			// signType: data.signType, //签名算法，暂支持 MD5
			// paySign: data.paySign, // 签名
			success: res => {
				console.log(res);
				this.paySuccess(res);
			},
			fail: err => {
				console.log(err);
				this.payFail(err);
			},
			complete: res => {
				console.log(res);
				this.payComplete(res);
			}
		});
		
	},
	
	wxappPay(data){
		
		// #ifdef APP-PLUS
			let weixinorder = this.getPayInfo(data);
			console.log(weixinorder);
			uni.requestPayment({
				provider: 'wxpay',
				orderInfo: weixinorder, // 订单数据
				success: res => {
					this.paySuccess(res);
				},
				fail: err => {
					this.payFail(err);
				},
				complete: res => {
					this.payComplete(res);
				}
			});
			
		// #endif
		// #ifdef MP-WEIXIN
			uni.requestPayment({
				provider: 'wxpay',
				orderInfo: '', // 订单数据
				timeStamp: data.timeStamp.toString(), // 时间戳从1970年1月1日至今的秒数，即当前的时间
				nonceStr: data.nonceStr, // 随机字符串，长度为32个字符以下
				package: data.package, // 统一下单接口返回的 prepay_id 参数值，提交格式如：prepay_id=xx
				signType: data.signType, //签名算法，暂支持 MD5
				paySign: data.paySign, // 签名
				success: res => {
					this.paySuccess(res);
				},
				fail: err => {
					this.payFail(err);
				},
				complete: res => {
					this.payComplete(res);
				}
			});
		// #endif
	},
	
	
	//二次签名
	getPayInfo (orderInfo) {
		console.log(orderInfo);
		let res = orderInfo; // 后台返回的统一下单数据
		let key = ""; // 加密Key，微信支付填写的key（后台提供）
		let payInfo = {
				appid: res.appId,
				noncestr:res.nonceStr,
				package: res.package,
				partnerid: "1606296765",
				prepayid: res.prepayid,
				timestamp: Number(res.timeStamp),
			}
		
		
		
		// 键值对按照ASCII码从小到大排序生成类似：appid=xxx&body=xx&device_info=1000
		let keyValueStr = this.mapObjToKeyValue(payInfo, true);
		
		// 插入加密Key到最后
		let strSignTemp = `${keyValueStr}&key=${key}`;
		// 真正的二次加密（需要引入md5.js源码，小编文章最后会附）
		let sign = md5(strSignTemp).toUpperCase().substr(0, 30);
		
		payInfo.sign = sign;
		// 返回字符串给uniapp调起支付用
		return JSON.stringify(payInfo);
	},
	/*
	 * 根据object生成key value字符串
	 * @params obj: any 要map的对象
	 * @params isSort: boolean 是否根据ASCII字典排序
	 */
	mapObjToKeyValue(obj, isSort = false) {
		let keys = Object.keys(obj);
		let str = "";
		
		if (isSort) keys.sort();
		keys.forEach(key => {
			if (obj.hasOwnProperty(key)) {
				str += `${key}=${obj[key]}&`;
			}
		});
		return str.replace(/&$/, "");
	},
	
	paySuccess(res){
		if(this.payData.success){
			this.payData.success(res);
		}
	},
	payFail(err){
		console.log(err);
		toast('支付取消');
		if(this.payData.fail){
			this.payData.fail(err);
		}
	},
	payComplete(res){
		// this.hideLoading();
		
	}
}