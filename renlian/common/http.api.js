// 如果没有通过拦截器配置域名的话，可以在这里写上完整的URL(加上域名部分)
let hotSearchUrl = '/ebapi/store_api/hot_search';
let indexUrl = '/ebapi/public_api/index';

// 此处第二个参数vm，就是我们在页面使用的this，你可以通过vm获取vuex等操作，更多内容详见uView对拦截器的介绍部分：
// https://uviewui.com/js/http.html#%E4%BD%95%E8%B0%93%E8%AF%B7%E6%B1%82%E6%8B%A6%E6%88%AA%EF%BC%9F
const install = (Vue, vm) => {
	
	//	参数(id)	获取轮播图
	let getSlide = (options = {}) => vm.$u.get('home/slides/' + options.id);
	
	//	参数(id)	获取轮播图
	let getNav = (options = {}) => vm.$u.get('home/nav/read/id/' + options.id);
	
	// 获取用户信息
	let getUserInfo = (options = {}) => vm.$u.get('user/profile/userInfo',options);
	
	// 获取用户信息
	let postUserInfo = (options = {}) => vm.$u.post('user/profile/userInfo',options);
	
	
	let appWXLogin = (res,callback) => {
		uni.login({
			provider: res.provider,
			success: loginRes => {
				if(loginRes.errMsg == "login:ok"){
					uni.getUserInfo({
						provider: res.provider,
						success: function(infoRes) {
							vm.$u.post("user/app/login", infoRes.userInfo).then(res => {
								callback(res);
							})
						}
					});
				}
			},
			complete(res) {
				console.log(res);
			}
		})
	}
	
	let login = (option) => {
		uni.getUserProfile({
			desc:"获取用户信息",
			success:res => {
				console.log(res);
				if (!res.iv) {
					vm.$u.toast("您取消了授权,登录失败")
					return false;
				}
				uni.login({
					provider: res.provider,
					success: loginRes => {
						if(loginRes.code){
							vm.$u.post("wxapp/public/login",{
								code:loginRes.code,
								encrypted_data:res.encryptedData,
								iv: encodeURIComponent(res.iv,"UTF-8"),
								raw_data: res.rawData,
								signature: res.signature,
							}).then(result => {
								if (result.code == 1) {
									if(option.success){
										option.success(result);
									}
								}else{
									vm.$u.toast(result.msg)
								}
							})
						}
					}
				})
			}
		})
	}
	
	let getPhoneNumber = (options) => {
		if (options.data.errMsg == "getPhoneNumber:fail user deny") return;
		uni.showLoading();
		uni.login({
		    success: res => {
		        if (res.code) {
					let phoneData = {
						'code': res.code,
						'encrypted_data': options.data.encryptedData,
						'iv': options.data.iv,
					};
					vm.$u.http.post("wxapp/public/getPhoneNumber",phoneData).then(data => {
						
						if(data.code){
							var user = uni.getStorageSync('user');
							if(user){
								user.mobile = data.data.user.mobile;
								uni.setStorage({ key: 'user', data: user,success:res => {
									if(options.success){
										options.success(data);
									}
								}})
							}else{
								if(options.success){
									options.success(data);
								}
							}
						}else{
							vm.$u.toast("绑定失败请重试");
						}
					})
		        }
		    }
		})
	}
	
	let bindwechat = (res) => {
		if (!res.data.iv) {
			uni.$u.toast("您取消了授权,登录失败");
			return false;
		}
		var t = this;
		
		uni.login({
			provider: 'weixin',
			success: loginRes => {
				if(loginRes.code){
					vm.$u.post("wxapp/public/bindwechat",{
						code:loginRes.code,
						encrypted_data:res.data.encryptedData,
						iv: res.data.iv,
						raw_data: res.data.rawData,
						signature: res.data.signature,
					}).then(result => {
						if (result.code == 1) {
							if(res.success){
								res.success(result);
							}
						}else{
							uni.showToast({
								title: result.msg,
								icon: "none"
							});
						}
					})
				}
			}
		})
	}
	
	let uploadFile = options => {
		var config = vm.$u.http.config;
		options.url = config.baseUrl + '/' + options.url;
		// options.header = config.header;
		var header = {};
		header['XX-Device-Type'] = config.header['XX-Device-Type'];
		header['XX-Token'] = uni.getStorageSync('token');
		options.header = header;
		let oldSuccess  = options.success;
		options.success = function (res) {
			console.log(res.data);
			let data = JSON.parse(res.data);
			console.log(data);
			if (data.code == 0 && data.data && data.data.code && data.data.code == 10001) {
				that.login();
			} else {
				oldSuccess(data);
			}
		}
		uni.uploadFile(options);
	}
	
	let fomartTime = value => {
	    let unit=['分','时','天'],
	    day=0,hour=0,min=0,second=0,returnStr=0,
	    arrVal=value.toString().split(".");
	    if(arrVal.length>1){
	       second=parseFloat("0."+arrVal[1]);
	       second*=60;
	       value=parseInt(arrVal[0]);
	    }
	    returnStr=value+unit[0];
	    if(value>=60){
	        hour=parseInt(value/60);
	        min=value%60;
	    }
	    if(hour){
	       returnStr=hour+unit[1];
	       if(min){
	           returnStr+=min+unit[0]
	       }
	    }
	    if(second){
	        returnStr+=second.toFixed(0)+'秒'
	    }
	     return returnStr
	}
	
	
	
	
	/*	 门户接口开始	*/
		let portal = {};
		//	热门推荐文章
		portal.getHotArticle = (options = {}) => vm.$u.get('portal/lists/recommended',options);
	/*	 门户接口结束	*/
	
	
	/*	 商城接口开始	*/
		let mall = {};
		//	(get)	商品列表
		mall.goodsList = (options = {}) => vm.$u.get('mall/goods/list',options);
		// mall.hotGoodsList = (options = {}) => vm.$u.get('mall/goods/list',options);
		//	(get)	商品规格
		mall.getGoodsSpec = (options = {}) => vm.$u.get('mall/goods/getSpec',options);
	/*	 商城接口结束	*/
	
	
	
	/*	 订单接口开始	*/
		let order = {};
		//	(get)		购物车列表
		order.getCart = (options = {}) => vm.$u.get('order/cart/cart',options);
		//	(post)		添加到购物车
		order.addCart = (options = {}) => vm.$u.post('order/cart/add',options);
		//	(delete)	删除购物车
		order.delCart = (options = {}) => vm.$u.delete('order/cart/delete',options);
		
		//	(get)		计算购物车价格总计
		order.calculation	= (options = {}) => vm.$u.get('order/cart/cart2',options);
		//	(get)		选中购物车商品
		order.selected 		= (options = {}) => vm.$u.get('order/cart/selected',options);
		//	(get)		选中全体购物车商品
		order.allselected	= (options = {}) => vm.$u.get('order/cart/allselected',options);
		
		
		//	(post)		选中全体购物车商品
		order.createOrder	= (options = {}) => vm.$u.post('order/cart/cart2',options);
		//	(get)		获取订单列表
		order.list			= (options = {}) => vm.$u.get('order/order/my',options);
		//	(get)		获取订单详情
		order.info			= (id) => vm.$u.get('order/detail/'+id);
		
		order.orderOperation = (options) => vm.$u.post('order/order/save',options);
		
	/*	 订单接口结束	*/
	
		
		
	
	
	/*	 用户接口开始	*/
		let user = {};
		//	(get)		获取默认收货地址
		user.getDefaultAddress = (options = {}) => vm.$u.get('user/address/default',options);
		//	(post)		设置默认收货地址
		user.setDefaultAddress = (options = {}) => vm.$u.post('user/address/isdefault',options);
		//	(get)		获取用户数据
		user.getUserInfo = (options = {}) => vm.$u.get('user/profile/userInfo',options);
		
	/*	 用户接口结束	*/
	
	
	let tablist = [];
	let userinfo = uni.getStorageSync('user');
	if(userinfo.user_type == 2){
		tablist = [
			{
				iconPath: "/static/iconfont/statistics.png",
				selectedIconPath: "/static/iconfont/statistics_selected.png",
				text: '统计',
				customIcon: false,
				pagePath:'/pages/statistics/statistics'
			},
			{
				iconPath: "/static/iconfont/my.png",
				selectedIconPath: "/static/iconfont/my_selected.png",
				text: '我的',
				customIcon: false,
				pagePath:'/pages/my/my'
			}
		];
	}else{
		tablist = [
			{
				iconPath: "/static/iconfont/home.png",
				selectedIconPath: "/static/iconfont/home_selected.png",
				text: '首页',
				customIcon: false,
				pagePath:'/pages/index/index'
			},
			{
				iconPath: "/static/iconfont/my.png",
				selectedIconPath: "/static/iconfont/my_selected.png",
				text: '我的',
				customIcon: false,
				pagePath:'/pages/my/my'
			}
		];
	}
	
	
	vm.$u.api = {
		getSlide,
		getNav,
		getUserInfo,
		postUserInfo,
		appWXLogin,
		login,
		getPhoneNumber,
		uploadFile,
		bindwechat,
		portal,
		mall,
		order,
		user,
		fomartTime,
		tablist: tablist
	};
}

export default {
	install
}