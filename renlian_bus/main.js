import App from './App'

// #ifndef VUE3

	import Vue from 'vue'
	Vue.config.productionTip = false
	App.mpType = 'app'
	// 引入全局uView
	import uView from 'uview-ui';
	Vue.use(uView);

	import store from '@/store';
	let vuexStore = require('@/store/$u.mixin.js');
	Vue.mixin(vuexStore);

	// 公共SDK专用
	import sdk from 'common/common.sdk.js';
	Vue.prototype.sdk = sdk;

	import Chinese from '@/common/locales/zh.js';
	import English from '@/common/locales/en.js';

	const app = new Vue({
		store,
		...App
	});

	// http拦截器，将此部分放在new Vue()和app.$mount()之间，才能App.vue中正常使用
	import httpInterceptor from '@/common/http.interceptor.js';
	Vue.use(httpInterceptor, app);

	// http接口API抽离，免于写url或者一些固定的参数
	import httpApi from '@/common/http.api.js';
	Vue.use(httpApi, app);
	app.$mount()
	
	
// #endif





// #ifdef VUE3
import { createSSRApp } from 'vue'
export function createApp() {
  const app = createSSRApp(App)
  return {
    app
  }
}
// #endif